import { API_BASE_URL } from "../consts/config";
import { requests } from "../utils/requestsHandler";

export const userService = {
  signUp: user => requests.post(`${API_BASE_URL}/signup`, user) 
}
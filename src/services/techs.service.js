import { API_BASE_URL } from "../consts/config";
import { requests } from "../utils/requestsHandler";

export const techsService = {
  getAll: () => requests.get(`${API_BASE_URL}/techs`) 
}
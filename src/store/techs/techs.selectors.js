import { sortAsc, sortDesc } from "../../utils/techsOrder"

export const currentTechs = store => {
  let techs = store.techs.list
  if (store.techs.search) {
    techs = techs.filter(item => item.tech.toUpperCase().includes(store.techs.search.toUpperCase()))
  }
  if (store.techs.type) {
    techs = techs.filter(item => item.type === store.techs.type)
  }
  if (store.techs.order) {
    techs = store.techs.order === "asc" ? techs.slice().sort(sortAsc) : techs.slice().sort(sortDesc)
  } 
  return techs;
}

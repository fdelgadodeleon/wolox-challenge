import { techsService } from '../../services/techs.service';
import TECHS_ACTION_TYPES from './techs.types';

export const fetchTechsStart = () => ({
  type: TECHS_ACTION_TYPES.FECTH_TECHS_START
});

export const fetchTechsSuccess = (techs) => ({
  type: TECHS_ACTION_TYPES.FECTH_TECHS_SUCCESS,
  payload: { techs }
});

export const fetchTechsFailure = (error) => ({
  type: TECHS_ACTION_TYPES.FETCH_TECHS_FAILURE,
  payload: { error }
});

export const search = (search) => ({
  type: TECHS_ACTION_TYPES.SEARCH,
  payload: {search}
})

export const clearSearch = () => ({
  type: TECHS_ACTION_TYPES.SEARCH,
  payload: {}
})

export const filterByType = type => ({
  type: TECHS_ACTION_TYPES.FILTER_BY_TYPE,
  payload: {type}
})

export const clearTypeFilter = () => ({
  type: TECHS_ACTION_TYPES.FILTER_BY_TYPE,
  payload: {}
})

export const orderByName = order => ({
  type: TECHS_ACTION_TYPES.ORDER_BY_NAME,
  payload: {order}
})

export const clearOrder = () => ({
  type: TECHS_ACTION_TYPES.ORDER_BY_NAME,
  payload: {}
})

export const addFav = (tech) => ({
  type: TECHS_ACTION_TYPES.ADD_FAV,
  payload: {tech}
})

export const removeFav = (tech) => ({
  type: TECHS_ACTION_TYPES.REMOVE_FAV,
  payload: {tech}
})

export const fetchTechs = () => dispatch => {
  dispatch(fetchTechsStart())
  techsService.getAll()
    .then(res => dispatch(fetchTechsSuccess(res)))
    .catch(err => {
      dispatch(fetchTechsFailure(err))
      return Promise.reject()
    })
}
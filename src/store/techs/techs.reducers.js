import TECHS_ACTION_TYPES from './techs.types';

export const INITIAL_STATE = {
  list: [],
  error: null,
  loading: false,
  search: "",
  type: "",
  order: "",
  favList: []
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case TECHS_ACTION_TYPES.FECTH_TECHS_START:
      return {
        ...state,
        list: [],
        error: null,
        loading: true
      };
    case TECHS_ACTION_TYPES.FECTH_TECHS_SUCCESS:
      return {
        ...state,
        list: action.payload.techs,
        error: null,
        loading: false
      };
    case TECHS_ACTION_TYPES.FETCH_TECHS_FAILURE:
      return {
        ...state,
        list: [],
        error: action.payload.error,
        loading: false
      };
    case TECHS_ACTION_TYPES.SEARCH:
      return {
        ...state,
        search: action.payload.search
      }
    case TECHS_ACTION_TYPES.FILTER_BY_TYPE:
      return {
        ...state,
        type: action.payload.type
      }
    case TECHS_ACTION_TYPES.ORDER_BY_NAME:
      return {
        ...state,
        order: action.payload.order
      }
    case TECHS_ACTION_TYPES.REMOVE_FAV:
      return {
        ...state, 
        favList: state.favList.filter(tech => tech !== action.payload.tech)
      }
    case TECHS_ACTION_TYPES.ADD_FAV:
      let techs = []
      if (state.favList) {
        techs = state.favList.slice()
        techs.push(action.payload.tech)
      } else {
        techs = [action.payload.tech]
      }
      return {
        ...state, 
        favList: techs
      }
    default:
      return state;
  }
};

export default userReducer;
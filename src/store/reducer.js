import { combineReducers } from 'redux';

import userReducer from './user/user.reducers';
import techsReducer from './techs/techs.reducers';

const reducer = combineReducers({
  user: userReducer,
  techs: techsReducer,
});

export default reducer;
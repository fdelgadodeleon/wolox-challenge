import { userService } from '../../services/user.service';
import USER_ACTION_TYPES from './user.types';

export const signUpSuccess = ({ token }) => ({
  type: USER_ACTION_TYPES.SIGN_UP_SUCCESS,
  payload: { token }
});

export const signUpFailure = (error) => ({
  type: USER_ACTION_TYPES.SIGN_UP_FAILURE,
  payload: { error }
});

export const logoutSuccess = () => ({
  type: USER_ACTION_TYPES.LOGOUT_SUCCESS,
});

export const signUp = user => dispatch => {
  return userService.signUp(user)
    .then(res => dispatch(signUpSuccess(res)))
    .catch(err => {
      dispatch(signUpFailure(err))
      return Promise.reject()
    })
}

export const logout = () => dispatch => {
  dispatch(logoutSuccess())
}
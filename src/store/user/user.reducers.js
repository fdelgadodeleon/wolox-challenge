import USER_ACTION_TYPES from './user.types';

export const INITIAL_STATE = {
  token: null,
  error: null
};

const userReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_ACTION_TYPES.SIGN_UP_SUCCESS:
      return {
        ...state,
        token: action.payload.token,
        error: null
      };
    case USER_ACTION_TYPES.SIGN_UP_FAILURE:
      return {
        ...state,
        error: action.payload.error
      };
    case USER_ACTION_TYPES.LOGOUT_SUCCESS:
      return {
        ...state,
        token: null,
        error: null
      };
    default:
      return state;
  }
};

export default userReducer;
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducer';
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage' 
import { createWhitelistFilter } from 'redux-persist-transform-filter';

const persistConfig = {
  key: 'root',
  storage,
  transforms: [
    createWhitelistFilter('user', ['token']),
    createWhitelistFilter('techs', ['favList'])
  ]
}
 
const persistedReducer = persistReducer(persistConfig, rootReducer)
 
const configureStore = () => {
  let store = createStore(persistedReducer, applyMiddleware(thunk))
  let persistor = persistStore(store)
  return { store, persistor }
}

export default configureStore;
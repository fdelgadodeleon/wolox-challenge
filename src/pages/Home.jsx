import React from 'react';
import Cover from '../components/home/Cover';
import Banner from '../components/home/Banner';
import Benefits from '../components/home/Benefits';
import Footer from '../components/home/Footer';

const Home = () => {
  return (
    <div id="home">
      <Cover/>
      <Banner/>
      <Benefits/>
      <Footer/>
    </div>
  )
}

export default Home;
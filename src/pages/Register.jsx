import React from 'react';
import RegisterForm from '../components/register/RegisterForm';

const Register = () => {
  return (
    <div className="container">
      <h1>Crea una cuenta</h1>
      <RegisterForm/>
    </div>
  )
}

export default Register;
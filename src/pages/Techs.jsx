import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Spinner from '../components/common/Spinner';
import TechFilter from '../components/techs/TechFilter';
import TechList from '../components/techs/TechList';
import CustomText from '../components/common/CustomText';
import { fetchTechs } from '../store/techs/techs.actions';
import { currentTechs } from '../store/techs/techs.selectors';

const Techs = () => {
  const dispatch = useDispatch()
  const loading = useSelector(store => store.techs.loading)
  const filteredTechs = useSelector(currentTechs)
  const techs = useSelector(store => store.techs.list)

  useEffect(() => {
    if (!loading && (!techs || techs.length === 0)) {
      dispatch(fetchTechs())
    }
  }, [dispatch, techs, loading])
  
  return (
    <div className="container">
      <TechFilter />
      {loading ? (
        <div className="tech-spinner-container">
          <Spinner/>
        </div>
      ) : (
        <div className="tech-list-container">
          <TechList/>
          {filteredTechs && filteredTechs.length > 0 && (
            <div className="center">
              <CustomText variant="secondary" weight="bold">Total: {filteredTechs.length}</CustomText>
            </div>
          )}
        </div>
      )}
    </div>
  )
}

export default Techs;
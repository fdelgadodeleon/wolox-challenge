import hourIcon from '../assets/images/Ic_Hour.svg';
import homeOfficeIcon from '../assets/images/Ic_HomeOffice.svg';
import workshopsIcon from '../assets/images/Ic_Workshops.svg';
import drinkSnacksIcon from '../assets/images/Ic_DrinkSnacks.svg';
import laptopIcon from '../assets/images/Ic_laptop.svg';
import brainIcon from '../assets/images/Ic_brain.svg';

export const benefits = [
  {
    title: "Flexibilidad horaria",
    icon: hourIcon
  },
  {
    title: "Home Office",
    icon: homeOfficeIcon
  },
  {
    title: "Capacitaciones y workshops",
    icon: workshopsIcon
  },
  {
    title: "Snacks, frutas y bebidas gratis",
    icon: drinkSnacksIcon
  },
  {
    title: "Semana remota",
    icon: laptopIcon
  },
  {
    title: "Trabajar en últimas tecnologías",
    icon: brainIcon
  }
]
export const countries = [
  {
    name: "Argentina",
    provinces: [
      "Buenos Aires",
      "Córdoba",
      "Mendoza",
      "Santa Fe",
      "La Pampa"
    ],
  },
  {
    name: "Uruguay",
    provinces: [
      "Montevideo",
      "Canelones",
      "Maldonado",
      "Rocha",
      "Rivera"
    ]
  },
  {
    name: "Colombia",
    provinces: [
      "Bolívar",
      "Boyacá", 
      "Caldas", 
      "Cauca",
      "Magdalena"
    ]
  },
  {
    name: "Chile",
    provinces: [
      "Melipilla",
      "Talagante",
      "Cachapoal",
      "Iquique",
      "Santiago"
    ]
  },
  {
    name: "Perú",
    provinces: [
      "Lima",
      "Ica",
      "Cusco",
      "Cajamarca",
      "Ayacucho"
    ]
  }
];
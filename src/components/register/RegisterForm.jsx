import React, {useState} from 'react';
import { Formik, Field, Form } from 'formik';
import CustomFormInput from '../common/CustomFormInput';
import CustomFormSelect from '../common/CustomFormSelect';
import CustomButton from '../common/CustomButton';
import { countries } from '../../consts/countries';
import * as Yup from 'yup';
import { useDispatch, useSelector } from 'react-redux';
import { signUp } from '../../store/user/user.actions';
import { useHistory } from 'react-router-dom';
import Spinner from '../common/Spinner';

const SIGNUP_SCHEMA = Yup.object().shape({
  firstName: Yup.string()
    .required('Campo requerido')
    .max(30, 'Demasiado largo!'),
  lastName: Yup.string()
    .required('Campo requerido')
    .max(30, 'Demasiado largo!'),
  country: Yup.string()
    .required('Campo requerido'),
  province: Yup.string()
    .required('Campo requerido'),
  mail: Yup.string()
    .email('Email invalido')
    .required('Campo requerido'),
  phone: Yup.number()
    .required("Campo requerido")
    .positive("Teléfono inválido")
    .integer("Teléfono inválido")
    .max(9999999999, 'Demasiado largo!'),
  password: Yup.string()
    .required("Campo requerido")
    .min(6, 'Demasiado corta!'),
  passwordConfirmation: Yup.string()
    .required("Campo requerido")
    .oneOf([Yup.ref('password'), null], "Las contraseñas no coinciden")
    .min(6, 'Demasiado corta!'),
  accept: Yup.bool().oneOf([true], 'Debe aceptar los términos y condiciones')
});

const RegisterForm = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const error = useSelector(state => state.user.error)
  const [provinceOptions, setProvinceOptions] = useState([])

  const countryOptions = countries.map(country => ({value: country.name, label: country.name}))

  const handleCountryChange = (event, form) => {
    const selected = event.target.value
    form.setFieldValue('country', selected)
    form.setFieldValue('province', '')
    if (selected) {
      const currentCountry = countries.find(country => country.name === selected)
      setProvinceOptions(currentCountry ? 
        currentCountry.provinces.map(province =>({value: province, label: province}) ) 
        : [])
    } else {
      setProvinceOptions([])
    }
  }

  const handleSubmit = values => (
    dispatch(signUp({
      firstName: values.firstName,
      lastName: values.lastName,
      country: values.country,
      province: values.province,
      mail: values.mail,
      phone: values.phone,
      password: values.password
    })).then(() => history.push('/techs'))
  )

  const goBack = () => {
    history.goBack()
  }

  return (
    <Formik
      initialValues={{
        firstName: '',
        lastName: '',
        country: '',
        province: '',
        mail: '',
        phone: '',
        password: '',
        passwordConfirmation: '',
        accept: false
      }}
      validationSchema={SIGNUP_SCHEMA}
      onSubmit={handleSubmit}
      validateOnMount
    >
      {({ isValid, isSubmitting }) => (
        <div>
          {error && <p className="form-error">{error}</p>}
          <Form>
            <div className="row">
              <div className="short-input">
                <Field name="firstName">
                  {({ field, meta }) => (
                    <CustomFormInput 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched} 
                      label="Nombre *"
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
              <div className="short-input">
                <Field name="lastName">
                  {({ field, meta }) => (
                    <CustomFormInput 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched} 
                      label="Apellido *"
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
            </div>
            <div className="row">
              <div className="short-input">
                <Field name="country">
                  {({ field, meta, form }) => (
                    <CustomFormSelect 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched}
                      label="País *" 
                      onChange={event => handleCountryChange(event, form)}
                      options={countryOptions}
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
              <div className="short-input">
                <Field name="province">
                  {({ field, meta }) => (
                    <CustomFormSelect 
                      {...field} 
                      error={meta.error}
                      touched={meta.touched}
                      label="Provincia *" 
                      options={provinceOptions}
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
            </div>
            <div className="row">
              <div className="large-input">
                <Field name="mail">
                  {({ field, meta }) => (
                    <CustomFormInput 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched} 
                      label="Email *" 
                      type="email"
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
            </div>
            <div className="row">
              <div className="large-input">
                <Field name="phone">
                  {({ field, meta }) => (
                    <CustomFormInput 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched} 
                      label="Teléfono *" 
                      type="number"
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
            </div>
            <div className="row">
              <div className="short-input">
                <Field name="password">
                  {({ field, meta }) => (
                    <CustomFormInput 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched} 
                      label="Contraseña *" 
                      type="password"
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
              <div className="short-input">
                <Field name="passwordConfirmation">
                  {({ field, meta }) => (
                    <CustomFormInput 
                      {...field} 
                      error={meta.error} 
                      touched={meta.touched} 
                      label="Repetir contraseña *" 
                      type="password"
                      disabled={isSubmitting}
                    />
                  )}
                </Field>
              </div>
            </div>
            <div className="row">
              <Field name="accept">
                {({ field, meta }) => (
                  <label className={`${meta.touched && meta.error ? 'error-checkbox' : ''}`}>
                    <input type="checkbox" {...field}/>
                    <a href="/terms-and-conditions" target="_blank"> Acepto términos y condiciones</a>
                  </label>
                )}
              </Field>
            </div>
            <div className="row buttons-container">
              <CustomButton 
                variant="outlined" 
                color="primary" 
                disabled={isSubmitting}
                onClick={goBack}
              >Cancelar</CustomButton>
              <CustomButton type="submit" color="primary" disabled={isSubmitting || !isValid}>Guardar</CustomButton>
            </div>
          </Form>
          <div className={`${isSubmitting ? 'overlay' : 'hidden'}`}>
            <Spinner/>
          </div>
        </div>
      )}
    </Formik>
  )
}

export default RegisterForm;
import React, { useRef } from 'react';
import burgerMenu from '../../assets/images/burger-menu.png';
import { useClickOutside } from '../../hooks/useClickOutside';
import { menuItems } from '../../consts/menuItems';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../store/user/user.actions';
import star from '../../assets/images/star.svg';

const DropdownMenu = () => {
  const dropdownRef = useRef(null)
  const [visible, setVisible] = useClickOutside(dropdownRef, false)
  const history = useHistory()
  const location = useLocation()
  const token = useSelector(store => store.user.token)
  const favList = useSelector(store => store.techs.favList)
  const dispatch = useDispatch()

  const toggleMenu = () => {
    setVisible(!visible)
  }

  const closeMenu = () => {
    setVisible(false)
  }

  const goToRegister = () => {
    history.push('/register')
  }

  const doLogout = () => {
    dispatch(logout())
    closeMenu()
  }

  return (
    <div className="menu-container">
      <button onClick={toggleMenu} className="menu-trigger">
        <img width="30" src={burgerMenu} alt="menu"/>
      </button>
      <nav ref={dropdownRef} className={`menu ${visible ? 'active' : 'inactive'}`}>
        <ul>
          {menuItems && location.pathname === "/" && menuItems.map(item => {
            return <li key={item.label}><a href={item.link} onClick={closeMenu}>{item.label}</a></li>
          })}
          {token && location.pathname !== "/techs" && <li><Link to="/techs" onClick={closeMenu}>Tecnologías</Link></li>}
          {token && favList && favList.length > 0 && location.pathname === "/techs" && (
            <li>
              <div className="header-fav">
                <span>{favList.length}</span>
                <img src={star} alt="favs"/>
              </div>
            </li>
          )}
          <li>
            {token ? 
              <button onClick={doLogout}>Cerrar Sesión</button> :
              location.pathname !== "/register" && <button onClick={goToRegister}>Registrarse</button>
            }
          </li>
        </ul>
      </nav>
    </div>
  ) 
}

export default DropdownMenu;
import React, {useEffect, useState} from 'react';
import logo from '../../assets/images/logo_full_color.svg';
import CustomButton from './CustomButton';
import DropdownMenu from './DropdownMenu';
import { menuItems } from '../../consts/menuItems';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../../store/user/user.actions';
import star from '../../assets/images/star.svg';

const Header = () => {
  const [isTop, setIsTop] = useState(true)
  const history = useHistory()
  const location = useLocation()
  const token = useSelector(store => store.user.token)
  const favList = useSelector(store => store.techs.favList)
  const dispatch = useDispatch()
  
  useEffect(() => {
    document.addEventListener('scroll', () => {
      setIsTop(window.scrollY < 100)
    });
  })

  const goToRegister = () => {
    history.push('/register')
  }

  return (
    <nav>
      <div className={`navbar ${!isTop ? 'bg-white' : ''}`}>  
        <Link to="/" className="logo"><img src={logo} alt="logo"/></Link>
        <div className="links">
          {menuItems && location.pathname === "/" && menuItems.map(item => {
            return <a key={item.label} href={item.link}>{item.label}</a>
          })}
          {token && location.pathname !== "/techs" && <Link to="/techs">Tecnologías</Link>}
          {token && favList && favList.length > 0 && location.pathname === "/techs" && (
            <div className="header-fav">
              <span>{favList.length}</span>
              <img src={star} alt="favs"/>
            </div>
          )}
          {token ? 
            <CustomButton color="primary" variant="outlined" onClick={() => dispatch(logout())}>Cerrar Sesión</CustomButton> :
            location.pathname !== "/register" && <CustomButton color="primary" variant="outlined" onClick={() => goToRegister()}>Registrarse</CustomButton>
          }
        </div>
        <div className="dropdown-menu">
          <DropdownMenu/>
        </div>
      </div>
    </nav>
  )
}

export default Header;
import React, {useState} from 'react';

const CustomFormSelect = ({name, value, label, error, options, touched, onBlur, ...otherProps}) => {
  const [activeField, setActiveField] = useState(false)

  const handleFocus = () => {
    setActiveField(true)
  }

  const handleBlur = e => {
    if (onBlur) onBlur(e)
    setActiveField(false)
  }

  return (
    <div className="custom-form-input">
      <select
        id={name}
        className={`${touched && error ? 'form-input-error': ''}`}
        value={value}
        onFocus={handleFocus}
        onBlur={handleBlur}
        {...otherProps}
      >
        <option value=""></option>
        {options && options.map(option => <option value={option.value} key={option.value}>{option.label}</option>)}
        </select>
      <label className={`${value || activeField ? 'shrink' : ''} form-input-label`}>{label}</label>
      {touched && error && <span className="error">{error}</span>}
    </div>
  )
}

export default CustomFormSelect
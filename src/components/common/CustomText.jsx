import React from 'react';

const CustomText = ({children, color, variant, weight, inline}) => {

  const getStyles = () => {
    return `${color || ''} ${variant || ''} ${weight || ''} ${inline ? 'inline' : '' }`
  }

  return (
    <p className={`custom-text ${getStyles()}`}>{children}</p>
  )
}

export default CustomText;
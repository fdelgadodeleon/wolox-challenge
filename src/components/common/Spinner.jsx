import React from 'react';

const Spinner = ({color, size}) => {

  const getStyles = () => {
    const colorClass = color ? `sp-${color}` : ''
    switch (size) {
      case 'sm':
        return `${colorClass} sp-sm`
      case 'lg': 
        return `${colorClass} lg`
      default: 
        return `${colorClass} md`
    }
  }

  return <div className={`spinner ${getStyles()}`}></div>
}

export default Spinner;
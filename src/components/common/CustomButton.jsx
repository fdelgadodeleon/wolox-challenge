import React from 'react';

const CustomButton = ({children, color, variant, disabled, ...otherProps}) => {

  const getStyles = () => {
    switch (variant) {
      case 'outlined':
        return `outlined-${color}`
      case 'filled':
        return `filled-${color}`
      default:
        return `filled-${color}`
    }
  }

  return (
    <button {...otherProps} disabled={disabled} className={`button ${getStyles()} ${disabled ? 'disabled' : ''}`}>
      {children}
    </button>
  )
}

export default CustomButton;
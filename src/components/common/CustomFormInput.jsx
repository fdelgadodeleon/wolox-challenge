import React, {useState} from 'react';

const CustomFormInput = ({ onChange, label, value, error, touched, name, disabled, onBlur, ...otherProps }) => {
  const [activeField, setActiveField] = useState(false)

  const handleFocus = () => {
    setActiveField(true)
  }

  const handleBlur = e => {
    if (onBlur) onBlur(e)
    setActiveField(false)
  }

  return (
    <div className="custom-form-input">
      <input
        id={name}
        className={`${touched && error ? 'form-input-error': ''}`}
        onChange={onChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        value={value}
        disabled={disabled}
        {...otherProps}
      />
      <label className={`${value || activeField ? 'shrink' : ''} form-input-label ${disabled ? 'disabled' : ''}`}>{label}</label>
      {touched && error && <span className="error">{error}</span>}
    </div>
  )
}

export default CustomFormInput;
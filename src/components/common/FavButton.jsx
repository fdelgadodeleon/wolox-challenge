import React from 'react';
import filledStar from '../../assets/images/star.svg';
import broderStar from '../../assets/images/star_border.svg';

const FavButton = ({filled, onClick}) => {
  return (
    <button className="fav" onClick={onClick}>
      <img src={filled ? filledStar : broderStar} alt="fav"/>
    </button>
  )
}

export default FavButton;
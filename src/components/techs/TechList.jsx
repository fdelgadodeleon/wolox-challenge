import React from 'react';
import { useSelector } from 'react-redux';
import { currentTechs } from '../../store/techs/techs.selectors';
import Divider from '../common/Divider';
import TechItem from './TechItem';

const TechList = () => {
  const techs = useSelector(currentTechs)
  
  return (
    <div className="tech-list">
      {techs && techs.length > 0 ? 
        techs.map((tech, index) => (
          <div key={tech.tech}>
            <TechItem item={tech}/>
            { index < techs.length - 1 && <Divider/> }
          </div>
        )) : (
          <p className="center">No se encontraron tecnologías, vuelva a intentarlo!</p>
        )}
    </div>
  )
}

export default TechList;
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { removeFav, addFav } from '../../store/techs/techs.actions';
import FavButton from '../common/FavButton';

const TechItem = ({item}) => {
  const dispatch = useDispatch()
  const favList = useSelector(store => store.techs.favList)

  const handleFavClick = () => {
    isFav() ? dispatch(removeFav(item.tech)) : dispatch(addFav(item.tech))
  }

  const isFav = () => favList && favList.includes(item.tech)

  return (
    <div className="tech-container">
      <div className="tech-logo">
        <img className="rounded" alt={item.tech} src={item.logo} />
      </div>
      <div className="tech-info">
        <div className="tech-left">
          <p className="tech-title">{item.tech}</p>
          <p className="tech-subtitle">{`${item.author} ${item.year}`}</p>
          <p className="tech-subtitle">{item.license}</p>
        </div>
        <div className="tech-right">
          <div>
            <p>{item.type}</p>
            <p>{item.language}</p>
          </div>
          <div className="fav-button">
            <FavButton filled={isFav()} onClick={handleFavClick}/>
          </div>
        </div>
      </div>
    </div>
  )
}

export default TechItem;
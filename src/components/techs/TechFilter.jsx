import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { clearOrder, clearSearch, clearTypeFilter, filterByType, orderByName, search } from '../../store/techs/techs.actions';
import CustomFormSelect from '../common/CustomFormSelect';
import CustomFormInput from '../common/CustomFormInput';

const TechFilter = () => {
  const [searchValue, setSearchValue] = useState("")
  const [typeValue, setTypeValue] = useState("")
  const [orderValue, setOrderValue] = useState("")
  const loading = useSelector(store => store.techs.loading)
  const dispatch = useDispatch()
  const types = [
    {value: "Back-End", label: "Back-End"},
    {value: "Front-End", label: "Front-End"},
    {value: "Mobile", label: "Mobile"},
  ]
  const orders = [
    {value: "asc", label: "Nombre Ascendente"},
    {value: "desc", label: "Nombre Descendente"},
  ]

  const handleSearchChange = event => {
    setSearchValue(event.target.value)
    dispatch(search(event.target.value))
  }

  const handleTypeChange = event => {
    setTypeValue(event.target.value)
    dispatch(filterByType(event.target.value))
  }
  
  const handleOrderChange = event => {
    setOrderValue(event.target.value)
    dispatch(orderByName(event.target.value))
  }

  const clearFilters = () => {
    setTypeValue("")
    setSearchValue("")
    setOrderValue("")
    dispatch(clearSearch())
    dispatch(clearTypeFilter())
    dispatch(clearOrder())
  }

  return (
    <div className="tech-filter-container">
      <div>
        <CustomFormInput
          value={searchValue}
          name="search"
          placeholder="Buscar por nombre"
          onChange={handleSearchChange}
          disabled={loading}
        />
        <CustomFormSelect 
          options={types}
          value={typeValue}
          name="type"
          label="Elegí el tipo de tecnología"
          onChange={handleTypeChange}
          disabled={loading}
        />
        <CustomFormSelect 
          options={orders}
          value={orderValue}
          name="type"
          label="Orderar por"
          onChange={handleOrderChange}
          disabled={loading}
        />
      </div>
      <button onClick={clearFilters}>Borrar filtros</button>
    </div>
  )
}

export default TechFilter;
import React from 'react';
import CustomText from '../common/CustomText'
import illustration from '../../assets/images/Ic_ilustra_Hero.png';

const Cover = () => {
  return (
    <div className="cover">
      <div>
        <CustomText variant="title" weight="lighter">Bienvenida a tu </CustomText>
        <CustomText variant="title" weight="bold" inline> Entrevista Técnica </CustomText>
        <CustomText variant="title" weight="lighter" inline>en</CustomText>
        <CustomText variant="title" weight="bold" color="secondary"> Wolox</CustomText>
      </div>
      <div className="illustration">
        <img src={illustration} alt="il"/>
      </div>
    </div>
  )
}

export default Cover;
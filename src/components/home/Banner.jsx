import React from 'react';
import CustomText from '../common/CustomText';
import CustomButton from '../common/CustomButton';
import twitter from '../../assets/images/twitter.png';

const Banner = () => {

  const goToTwitter = () => {
    window.open("https://twitter.com/Wolox")
  }
  return (
    <div className="banner">
      <div className="left">
        <div>
          <CustomText variant="title" weight="bold" color="secondary" inline>350 + </CustomText>
          <CustomText variant="title" weight="bold" color="primary" inline>Woloxers</CustomText>
        </div>
        <div className="twitter">
          <img width="50" src={twitter} alt="twitter"/>
          <p> @Wolox</p>
        </div>
        <CustomButton variant="outlined" color="primary" onClick={goToTwitter}>Siguenos</CustomButton>
      </div>
      <div className="right">
        <div>
          <CustomText variant="title">Trabajamos para</CustomText>
          <CustomText variant="title" weight="bold" color="primary" inline>convertir </CustomText>
          <CustomText variant="title" weight="bold" color="secondary" inline>ideas </CustomText>
          <CustomText variant="title" inline>en</CustomText>
          <CustomText variant="title">productos.</CustomText>
        </div>
      </div>
    </div>
  )
}

export default Banner;
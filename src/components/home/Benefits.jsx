import React from 'react';
import CustomText from '../common/CustomText';
import { benefits } from '../../consts/benefits';
import BenefitItem from './BenefitItem';

const Benefits = () => {
  return (
    <div className="benefits" id="benefits">
      <CustomText variant="subtitle" inline>Entre los beneficios que ofrecemos se encuentran </CustomText>
      <CustomText variant="subtitle" inline color="primary">;)</CustomText>
      <div className="list">
        {benefits && benefits.map(benefit => (
          <BenefitItem item={benefit} key={benefit.title}/>
        ))}
      </div>
    </div>
  )
}

export default Benefits;
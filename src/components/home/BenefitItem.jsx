import React from 'react';

const BenefitItem = ({item}) => {
  return (
    <div className="item">
      <div className="icon">
        <img src={item.icon} alt={item.title}/>
      </div>
      <p>{item.title}</p>
    </div>
  )
}

export default BenefitItem;
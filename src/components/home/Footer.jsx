import React from 'react';
import CustomButton from '../common/CustomButton';
import CustomText from '../common/CustomText';
import logoBlack from '../../assets/images/Ic_Wolox_Footer.svg';

const Footer = () => {
  
  const goToWolox = () => {
    window.open("https://www.wolox.com.ar/")
  }

  return (
    <div className="footer">
      <CustomText variant="title" inline weight="bold">Gracias por </CustomText>
      <CustomText variant="title" inline weight="bold" color="primary">completar el ejercicio</CustomText>
      <CustomText variant="subtitle">Te invitamos a ver más información</CustomText>
      <CustomButton color="primary" variant="filled" onClick={goToWolox}>Conocer más</CustomButton>
      <div>
        <img src={logoBlack} alt="wolox"/>
      </div>
    </div>
  )
}

export default Footer;
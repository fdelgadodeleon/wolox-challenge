import React, { Suspense, lazy } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import PrivateRoute from './components/common/PrivateRoute';
import Spinner from './components/common/Spinner';
import ErrorBoundary from './components/common/ErrorBoundary';
import Header from './components/common/Header';
import { useSelector } from 'react-redux';

const Home = lazy(() => import('./pages/Home'));
const Register = lazy(() => import('./pages/Register'));
const Techs = lazy(() => import('./pages/Techs'));
const TermsAndConditions = lazy(() => import('./pages/TermsAndConditions'));

function App() {
  const token = useSelector(store => store.user.token)

  return (
    <Router>
      <ErrorBoundary>
        <Suspense fallback={<div className="container"><Spinner/></div>}>
          <Header/>
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
            <Route path="/register">
              { token ? <Redirect to={{ pathname: "/techs" }}/> : <Register/> }
            </Route>
            <Route path="/terms-and-conditions">
              <TermsAndConditions/>
            </Route>
            <PrivateRoute path="/techs">
              <Techs/>
            </PrivateRoute>
          </Switch>
        </Suspense>
      </ErrorBoundary>
    </Router>
  );
}

export default App;

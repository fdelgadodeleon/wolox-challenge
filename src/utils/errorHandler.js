export const errorHandler = error => {
  if (error.status) {
    switch (error.status) {
      case 500:
        return "Hubo un error en el servidor"
      case 400:
        return "Hubo un error en la consulta"
      case 401:
        return "El usuario no está autorizado"
      case 403:
        return "El usuario no tiene permisos"
      default:
        return "Hubo un error en la consulta"
    }
  } else {
    return "Hubo un error de red"
  }
};

import { runFetch } from "./fetchHandler";

export const requests = {
  get: (url, params) => {
    if (params) {
      url += `?${new URLSearchParams(params).toString()}`;
    }
    return runFetch(
      url,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
  },
  post: (url, params) => {
    return runFetch(
      url,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      })
  },
  put: (url, params) => {
    return runFetch(
      url,
      {
        method: "PUT",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify(params),
      })
  },
  delete: (url) => {
    return runFetch(
      url,
      {
        method: "DELETE",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
  },
};
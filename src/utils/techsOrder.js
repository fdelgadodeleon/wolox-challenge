export const sortAsc = (a, b) => {
  if (a.tech > b.tech) {
    return 1;
  }
  if (a.tech < b.tech) {
    return -1;
  }
  return 0;
}

export const sortDesc = (a, b) => {
  if (a.tech < b.tech) {
    return 1;
  }
  if (a.tech > b.tech) {
    return -1;
  }
  return 0;
}